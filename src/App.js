import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom'


import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Menu from './pages/Menu';
import ProductView from './pages/ProductView';
import NotFound from './pages/NotFound';
import Dashboard from './pages/Admin/AdminDashboard.js';
import Details from './pages/Admin/AdminProductView.js';
import Add from './pages/Admin/AddNewProduct.js';
import Update from './pages/Admin/UpdateProduct.js';
import NewAdmin from './pages/Admin/AddNewAdmin.js';



import './App.css';
import {UserProvider} from "./UserContext";

function App() {
	const [user, setUser] = useState({
	        id:null,
	        isAdmin: null
	      })
	const unsetUser = () => {
	  localStorage.clear();
	}
	useEffect(()=>{
	  if(localStorage.getItem('token')){
	      
	      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
	        headers : {
	          "Authorization" : `Bearer ${localStorage.getItem("token")}`
	        }
	      })
	      .then(res=>res.json())
	      .then(data=>{
	        setUser({
	            id:data._id,
	            isAdmin : data.isAdmin
	        })
	        // }
	      })
	  }
	}, [])
	return(
		<UserProvider value={{user, setUser, unsetUser}}>
		  <Router>
		      <AppNavbar />
		      <Container>
		        <Routes>
		        	<Route path="/" element={<Home />} />
		        	<Route path="/home" element={<Home />} />
			        <Route path="/login" element={<Login />} />
			        <Route path="/register" element={<Register />} />
			        <Route path="/logout" element={<Logout />} />
			        <Route path="/menu" element={<Menu />} />
			        <Route path='/products/:productId' element={<ProductView />}/> 
			        <Route path='*' element={<NotFound />}/>

					<Route path='/admin' element={<Dashboard />}/> 
					<Route path='/admin/products/:productId' element={<Details />}/>
					<Route path='/admin/products/:productId' element={<Details />}/>
					<Route path='/admin/addProduct' element={<Add />}/>
					<Route path='/admin/updateProduct/:productId' element={<Update />}/>
					<Route path='/admin/updateProduct/:productId' element={<Update />}/>
					<Route path='/admin/setAdmin' element={<NewAdmin />}/>

		        </Routes>
		      </Container>
		  </Router>
		</UserProvider>
	)

}

export default App;
