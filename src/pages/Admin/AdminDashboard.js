import Menu from './AdminMenuView'
import {useEffect, useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import {Button} from 'react-bootstrap';
import UserContext from '../../UserContext'

export default function AdminDashboard(){
    const { user } = useContext(UserContext);

    const [products, setProducts] = useState([]);
    useEffect(()=>{
        if(user.isAdmin){
                    fetch(`${process.env.REACT_APP_API_URL}/products/products`,{
          headers : {
            "Authorization" : `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
            setProducts(data.map((product=>{
            return(
                <Menu key={product._id} product={product}/>
        )
    })
            ))
        })
        }
    }, [])

    return(
        (user.isAdmin)?
        <>  
           <div className="text-center">
                <h2 className="text-center">Admin Dashboard</h2>
                <Link className="btn btn-primary btn-block" to = "/admin/addProduct">Add a new product</Link>
                <Link className="btn btn-primary btn-block" to = "/admin/setAdmin">Set a user as admin</Link>
           </div>
            {products}
        </>
        :
        <div className = "text-center">
            <h1>Restricted Page!!!</h1>

        </div>
    )
}