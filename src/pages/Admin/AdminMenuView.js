import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminMenuView({product}) {
    const {name, description, price, _id} = product;
   
    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                <Link className="btn btn-primary" to={`/admin/products/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
