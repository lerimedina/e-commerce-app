import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../../UserContext'
import Swal from 'sweetalert2'

export default function AdminProductView(){

	const { productId } = useParams();

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [status, setStatus] = useState("");
	const [id, setId] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity]= useState(0);

	function archive(productId){
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method : "PATCH",
			headers : {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			Swal.fire({
				icon : 'success',
				title : 'Successfully archived a product'
			})
			navigate("/admin")
		})

	}

	function activate(productId){
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`,{
			method : "PATCH",
			headers : {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			Swal.fire({
				icon : 'success',
				title : 'Successfully activated a product'
			})
			navigate("/admin")
		})

	}

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res=>res.json())
			.then(data=>{
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
				setStatus(data.isActive)
				setId(data._id)
			})
	}, [productId])


	return(
		(user.isAdmin)?
			<Container className="mt-5">
				<Row>
					<Col lg={{span:6, offset:3}}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>ID:</Card.Subtitle>
								<Card.Text>{id}</Card.Text>
								<Card.Subtitle>Status:</Card.Subtitle>
								<Card.Text>{status.toString()}</Card.Text>	
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								{
									(user.id&&user.isAdmin)?
										<div>
											<Link className="btn btn-primary" to={`/admin/updateProduct/${productId}`}>Update</Link>
											<Button variant="danger" block onClick ={()=>archive(productId)}>Archive</Button>
											<Button variant="primary" block onClick ={()=>activate(productId)}>Activate</Button>
										</div>
									:
										<Link className="btn btn-danger btn-block" to =	"/login">Login to purchase</Link>
								}		
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>

		:

		<div className = "text-center">
		    <h1>Restricted Page!!!</h1>
		    <p><Link to='/login'>Login</Link>to a non admin account.</p> 

		</div>
	)
}