import Menu from './AdminMenuView'
import {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {Button, Form} from 'react-bootstrap';
import UserContext from "../../UserContext";
import {useContext} from 'react';
import Swal from 'sweetalert2';

export default function AddNewAdmin(){

    const { user, setUser } = useContext(UserContext)

    const [email, setEmail]= useState("");
    const [isActive, setIsActive] = useState(false)

    const navigate = useNavigate();

    function addNewAdmin(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/setAsAdmin`,{
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem("token")}`

            },
            body : JSON.stringify({
                email: email
            })
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            Swal.fire({
              icon: 'success',
              title: 'User is now an admin'
            })
        navigate("/admin")
        })

        // Clear input fields
        setEmail("");
        

    }

    useEffect(()=>{
       if(email!==""){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [email])

    return(
        (user.isAdmin)?
            <Form onSubmit={(e)=>addNewAdmin(e)}>
                <h2 className="text-center">Set user as admin</h2>

                <Form.Group className = "mt-3"  controlId="email">
                    <Form.Label>Enter User Email: </Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter an existing email "
                        value={email}
                        onChange={e=>setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                {
                    isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn" className="mt-2">Make User as Admin</Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Make User as Admin</Button>

                }

            </Form>
            :
            <div>
                <h1>Page Not Found</h1>
            </div>
    )
}