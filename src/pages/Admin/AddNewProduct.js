import Menu from './AdminMenuView'
import {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {Button, Form} from 'react-bootstrap';
import UserContext from "../../UserContext";
import {useContext} from 'react';
import Swal from 'sweetalert2';

export default function AddNewProduct(){

    const { user, setUser } = useContext(UserContext)

    const [productName, setProductName]= useState("");
    const [productDescription, setProductDescription]= useState("");
    const [price, setPrice]= useState("");
    const [isActive, setIsActive] = useState(false)

    const navigate = useNavigate();

    function addProduct(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products`,{
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem("token")}`

            },
            body : JSON.stringify({
                name: productName,
                description : productDescription,
                price : price,
            })
        })
        .then(res=>res.json())
        .then(data=>{
            Swal.fire({
              icon: 'success',
              title: 'Product Added'
            })
        navigate("/admin")
        })

        // Clear input fields
        setProductName("");
        setProductDescription("");
        setPrice("");
        

    }

    useEffect(()=>{
       if(productName!==""&&productDescription!==""&&price!==""){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [productName, productDescription, price])

    return(
        (user.isAdmin)?
            <Form onSubmit={(e)=>addProduct(e)}>
                <h2 className="text-center">Create New Product</h2>

                <Form.Group className = "mt-3"  controlId="productName">
                    <Form.Label>Enter Product Name: </Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Product Name: "
                        value={productName}
                        onChange={e=>setProductName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className = "mt-3"  controlId="productDescription">
                    <Form.Label>Enter Product Description: </Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Product Name: "
                        value={productDescription}
                        onChange={e=>setProductDescription(e.target.value)}
                        required
                    />
                </Form.Group>
                
                <Form.Group className = "mt-3"  controlId="price">
                    <Form.Label>Enter Price: </Form.Label>
                    <Form.Control
                        type="number"
                        placeholder="Enter Price: "
                        value={price}
                        onChange={e=>setPrice(e.target.value)}
                        required
                    />
                </Form.Group>

                {
                    isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn" className="mt-2">Create Product</Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Create Product</Button>

                }

            </Form>
            :
            <div>
                <h1>Page Not Found</h1>
            </div>
    )
}