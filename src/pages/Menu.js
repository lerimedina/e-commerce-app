import Menu from '../components/Menucomp'
import {useEffect, useState} from 'react';

export default function Home(){
    const [products, setProducts] = useState([]);
    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then(res=>res.json())
        .then(data=>{
            setProducts(data.map((product=>{
            return(
                <Menu key={product._id} product={product}/>
        )
    })
            ))
        })
    }, [])

    return(
        <>    
            <div className="text-center">
                 <h2 className="text-center">Available Cupcakes</h2>
            </div>  
            {products}
        </>
    )
}