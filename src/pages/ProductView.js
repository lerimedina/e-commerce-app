import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function ProductView(){

	const { productId } = useParams();

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity]= useState(0);

	function checkout() {

			  	fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
					method: "POST",
		    		headers: {
		    			Authorization: `Bearer ${ localStorage.getItem('token') }`,
		    			"Content-Type": "application/json"
		    		},
		    		body: JSON.stringify({
		    			productId: productId,
		    			quantity: quantity
		    		})
				}).then(response => response.json()).then(data => data);
				Swal.fire({
			      title: 'Successfully Purchased',
			      text: 'Order has been successfully placed',
			      icon: 'success'
			    })
				navigate("/menu")
			
	}



	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res=>res.json())
			.then(data=>{
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
			})
	}, [productId])


	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>	
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Form.Group className = "mt-3"  controlId="quantity">
								<Form.Label>Quantity: </Form.Label>
								<Form.Control className="text-center"
									type ="number"
									value={quantity}
									min = "0"
									max = "10"
									onChange={e=>setQuantity(e.target.value)}
									required
								/>
							</Form.Group>
							{
								(user.id&&user.isAdmin!==true)?
									<Button variant="primary" block onClick ={()=>checkout()}>Purchase</Button>
								:
									<Link className="btn btn-danger btn-block" to =	"/logout">Login to a non admin account to purchase.</Link>
							}		
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}