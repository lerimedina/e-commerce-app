import {useState, useEffect, useContext} from 'react';
import{Form, Button} from 'react-bootstrap';
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom"; 
import Swal from 'sweetalert2'

export default function Login(){
	const { user, setUser } = useContext(UserContext)
	const [email, setEmail]= useState("");
	const [password, setPassword]= useState("");
	const [isActive, setIsActive] = useState(false)

	
	function loginUser(e){

		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				console.log(data.access);
				retrieveUserDetails(data.access)
				Swal.fire({
				  icon: 'success',
				  title: 'Login successful',
				  text: 'Welcome to the shop!'
				})
			}else{
				Swal.fire({
				  icon: 'error',
				  title: 'Authentication failed',
				  text: 'Please check your login credentials and try again'
				})
			}
		})

		setEmail("");
		setPassword("");
	}

	const retrieveUserDetails = (token) => {
		fetch("https://capstone-2-medina.onrender.com/users/userDetails",{
			headers : {
				"Authorization" : `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			localStorage.setItem('isAdmin', data.isAdmin);
			setUser({
				id:data._id,
				isAdmin	: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if(email!==""&&password!==""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id)?
			(user.isAdmin)?
				<Navigate to="/admin"/>
				:
				<Navigate to="/menu"/>
			:
			<Form onSubmit={(e)=>loginUser(e)}>
				<h2>Login</h2>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter Email"
						value={email}
						onChange={e=>setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter a password"
						value={password}
						onChange={e=>setPassword(e.target.value)}
						required
					/>
				</Form.Group>
				{
					isActive ? 
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Login</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Login</Button>

				}
			</Form>
	)
}