import{Row, Col, Card, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Higlights() {
	return(
		
		<Row className ="mb-3">
					<Col xs={12} md={4}>
						<Card className="cardHighlight p-3">
							<Card.Img variant="top" src="https://lh3.googleusercontent.com/fife/APg5EOZQCg9GKeXAy0XTU53UBEJ2V-sXLx-hdjztX_tNfCrv18MknYLsQYC0uuODsRYsgRYwcob4ELfntwgqyD4nD4QyTQKInO2bpu0JrxHbXGcl1tJ1umsR17hiUmNZC_Xz0ngHucnEtw3oSjw55YZnakc0_d-yiwMLjvQMgIvBlAwR9PSyG2o6RTPbTjLX9B81eMUSjxQUmQQm5pWdbTapBUjvIn0FPCEjHbnrI9Dv9gZzii9lTlbEKQHST2xRkrxTKsAGmmYoGkFMTfqfPxRBu2AIv-od44xWJmoAWVbXAwyeUJ48ebccQUJgQ5bmBfW0D0VKk3nice2l8GRweDuyPvOnfgwEMRboKvKPm_5qrMw91aJhK0Rn8RRXX2FzUjBwwIgZQ2RCdR3R0UwjT1ZhUoeN7A-iww9Ltm4KMTli_mAdm1K7rLyJ9Fv3baXgMOZv0X0ZiAouOdRUCJ1jhTHJgt28k-x830wXaJtarn6K9UoH8vEJCo7jV06yyWUJ-dKonaUojBLqkLAWETY0_ciKDkoJUX7nxyfZ5RscXW_hWGaqOXuTtSQQFs7sfy9abyOcfwf8rs1e84-NqPzwYgFWV9Vwxd03iTvR8QaojLA6JbSntSCs8u7JWa2NQmIv1CkOU-cPYhQMwigEDbbqo3Odltqbz5H7UOnlUcFINpvWi4HjrZ4wUZ3RqljiPVXcv0fMfH-dJiUxWBhUkAfzTnq0PsaTact1aj-SuenII4DidCA1FQuLUjNctxGXoc_dmJPlnh2pVJDJsBDFm2_gAugJ4cRXHklfVGIgagk0__qLsOfXSWOWgcwoVh5wGrVgYrMMf8s4n-bojSrlOOZ3V7mcHtGBiidnhNRl4_7h-aaUeXzbG6_W4N1Y-wC3ML6lAF9VvS6n8vG_lWjx-sqPAFbSt1oE2ym1IM5-sM7XBcOO2fM56YuqbJ1OtxzvTRSRQIzEHo-O6VI6YeWdOQiVKPHfq5qwlDnl3SDQyGpPScoaIz3hJu8Z6Ck8438XfbpR5Q1sN201PE0N__Z5oHIsDDW21GJQGnw3Oh4Vzbi45M_uCVxLn7dvlOVrBb1p5u3kmcviJk7mA-Y73CoLqFEwAVW6jQYxYihFL9K8SfiUolCt9uIz_snpiJfR1H_oQ8wrebY_zdthCnYPH-xVv6PBE73Sr5qNsK2S8cpPFebW8mwv3lT3nWzT8mhTFgRViZGV5dfQbkJY9dwsyEV0CbYM2lCtvB9otPSe81VhSpKDd9abFWr1Ovile7q43yd5ARtgNQnQlI5syR166Ecl6WUSYUfadHaV2DJ7Nrr3KMWKCyaadzX24pnjIXoNH66GgmUgJjlU9mEIrRV2U_D99IMIFTtpBNE8LCo-cBUmDlB3Oghq88P8fd8GAcYFme3uYpEIOeM-buMEZHciDojleo8WLIpYMYtz3Xc-wbnJYF9TvBzMttlFcspzFixiVkmo3-OeEL99MjnIXu4be88lvWZvYxATyOzNPfgCXqPnyq36PP58qVGGHvVx2GeWKAsHr1-VrlI25I09nmWwLohYDBvRhr5-GphT6B0VlgQuG5uKn942hrvTEERzkKzcH_gToYi28E5aECkcgTbSYBbfcbzUz5h9qVik_aFJrJPWQbymRT8B8E3TWCbhDJFp=w2000-h1982" />
							<Card.Body>
								<Card.Title>
									<h2>Red Velvet Cupcake</h2>
								</Card.Title>
								<Card.Text>
									Luscious red velvet cupcake with cream cheese frosting and chocolate shavings.
								</Card.Text>
							</Card.Body>
							<Link className="btn btn-success btn-block" to ="/menu">Check our menu</Link>
						</Card>
					</Col>

					<Col xs={12} md={4}>
						<Card className="cardHighlight p-3">
						<Card.Img variant="top" src="https://lh3.googleusercontent.com/fife/APg5EOb0b2_vLMqBnyoK7NuHbv1WN-q09ZWCfgRu3PH_oPk-eHtRjn_I5B6PuBL09xGGNWz6Z8306-ozpPX6IjDo-QYTwCX2dj6kHaQiUCrDp0VJ12kDa9guT_4vtqtClvlIz4fWPPscsA1f4XM4uXbAQbENdKcbXsMGlymNmINHh_ZqDYMbue5rMETCU3U2EMK8xD6CNr8ZC69O6CjPsNBxRMeuEEMDW6AzuNLxPOjzgD-ciCsOcnAdBAIWhTiYt1iS83EOwG__2JoGdlniprQwGimTifqsHlnTk-xvTaQK-uVFmNzqZ34iJU-u2TpFNLiDPo03zarg_2CjaLbTZJENEjKaGU5p4YAKnURU2o5g6oKHdKHGrcQf5WKRy9tDFBu_0pVvuYrafMk9Ndo_vDisb6u36vQYLCwAqlqoLHJuSnIPtwBxWNKUy-l_dXz4MujyaIBZAAFS2ut-oj4BTyP0qhiaYK-xDHc_d2I_THreM00mJ3RjW6Qv6K6PzQQ7HaPS02nja2aIFZVcnsQYO7W9UVSlVJ0JAdUo5EGxqldetLZFwanmCPlt0EqkWThSwu2pMLFn1tyyOVWwyUlSlBaBlv4-XHTk7I9wOGCOTI_YZUj-9HqQCQoa-kE0SjtZiO5N0SQWd3dG6EvkempFwn3xfPRuQ_gRESw2QC_ngzjfN8O5xeZQ94BufgV-sB2mNx0t8U7HFQHdqD0D6KdGnOK_R8EmmIjrrJzrjCAzgthy2S_Y2OdfRnQAMUMjlvi1JXiD4XT9EuHc0mF8kXTOSW8QW86DUELHj7ydq5eP9vb2OD8neY2we35bDJ8woXvlPUG0ogfEBZbnxVNeT7smwXZwiMPT3NAcTZupD6d-jZ2w0-Fhdn4x8WnbX6RI0xbY-FxRxzMGo6-wj3TrUiFFYrZ0OlQLSVmSb5Iud3CPtxIjqMo_izzUovp4BchIIXcjmSv3QcUhvldvzsrG_zYfi-r3qqfISaJHaUuoP0765abYgv5lBGex3xyCWp8vZITfy3p5ZDBpRPZnH1ZTw7EgIBX5zg2D3bURX1-jcc8crmtQUK8CWpQ4WhR7HDCcmJkL2wb5KVQUKv4WgywZGZkU71_6wgm-pORqDUYBERkm8hzIbMVGeI1bawNWtw2Qwmyekuf_i8YkRt5haNAONyZq7CFpLJZi_rh9USrPvRmQP7DeMzXJK14a3o08MZnpnZsJnUfbMFy8q0aI6PzcwSb84kBIHcw-NFR-MxWy3g1Cl32_36xe-YpuDgqwD4tJxPy7o5PSnuZV1_DFjuyFFCqpT8pL8H8GTZmuSbCwse7tv_GtM14q5uOfKMZWg0-DsNfO1vI1HC9YJUFhl6CinX_7xnoyIm_MvTjBVtu_37Ytb_6btA685pwBk8-Z02dJisrUbJU2Tyf3KRlt2KpnZ623Pzky_54ipm9ZDdMuTDUD5jBEH8jXjqI9h_rk8Zzu4yr3CvznN_EFBM9RFJAwccK2Co6OlYeHce2GzRxLDLLlmH4jbMOvfnxj1W-u-f-J_rk3AceeUr47h7OtcpVwCxx6cS8cTgPoHa6-oXAYWLzZCjQlL_pjZteorFASuUn5pyj27M3KNEL__q3usCSQquVn7hKeh6Yb9psDTRSSyvawGSglwfwiyUbiq84W=w2000-h1982" />
							<Card.Body>
								<Card.Title>
									<h2>Lemon Cupcake</h2>
								</Card.Title>
								<Card.Text>
									The baker's favorite tangy lemon cupcake with vanilla buttercream and lemon curd filling
								</Card.Text>
							</Card.Body>
							<Link className="btn btn-success btn-block" to ="/menu">Check our menu</Link>
						</Card>
					</Col>
					<Col xs={12} md={4}>
						<Card className="cardHighlight p-3">
						<Card.Img variant="top" src="https://lh3.googleusercontent.com/u/0/drive-viewer/AFGJ81pU4bcRTESyNlXB7XZMC8sD-g9V6b6qUEZ2VMnVYcQB1kLbkCijHM0DNlKRRK6pR1QcfzOsom7BW2zGlFlK07RBVOI9_Q=w2134-h2160" />
							<Card.Body>
								<Card.Title>
									<h2>Chocolate Cupcake</h2>
								</Card.Title>
								<Card.Text>
									A creamy airy chocolate cupcake with chocolate ganache frosting and chocolate droplets toppings
								</Card.Text>
							</Card.Body>
							<Link className="btn btn-success btn-block" to ="/menu">Check our menu</Link>
						</Card>
					</Col>
				</Row>
	)
	
}