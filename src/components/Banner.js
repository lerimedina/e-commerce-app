import {Button, Col, Row, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import logo from '../logo.png';

export default function Banner(){
	return(
		<>
		
		<Row>
			<Col className="text-center">
				<img class="center" src={logo} alt="Logo" />
				<p>Deliciously Perfect, Lusciously Marvelous</p>
			</Col>
		</Row>
		</>

	)
}