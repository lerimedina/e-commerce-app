import {useContext} from "react";
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import UserContext from "../UserContext";
import navbarlogo from '../navbarlogo.png';


export default function AppNavbar(){

	const { user } = useContext(UserContext)

	return(

		<Navbar bg="light" expand="lg">
			<Container fluid>
				{
					(user.isAdmin)?
						<Navbar.Brand
							as={NavLink} to="/"><img src={navbarlogo} width ="125" height ="31"/>
						</Navbar.Brand>
						:
						<Navbar.Brand
							as={NavLink} to="/"><img src={navbarlogo} width ="125" height ="31"/>
						</Navbar.Brand>
				}
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link as={NavLink} to="/menu">Menu</Nav.Link>
						{

							((user.id))?
								((user.isAdmin))?
								<>
									<Nav.Link as={NavLink} to="/admin">Dashboard</Nav.Link>
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>
								:
								<>
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>
							:
							<>
								<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
							</>
						}

					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}